package pl.poznan.putmotorsport.telemetria.android;

import android.content.Context;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.*;

import putmotorsport.poznan.pl.telemetria.android.R;

class TcpClient extends Thread implements AbstractTcpClient {
    private final Map<Integer, Data> dataMap;
    private final Random random;
    private Context context;
    private String address;
    private int port;
    private Collection<Integer> activeIds;
    private Socket socket;

    TcpClient(Context ctx) {
        context = ctx;
        address = ctx.getResources().getString(R.string.address);
        port = ctx.getResources().getInteger(R.integer.port);
        activeIds = Collections.emptyList();

        dataMap = new TreeMap<>();
        random = new Random();

        for (int id : Charts.identifiers)
            dataMap.put(id, new Data());
    }

    @Override
    public List<Integer> getNewData(int id) {
        // special case for random chart
        if (id == 0)
            return Collections.singletonList(random.nextInt(50));

        try {
            List<Integer> list = new ArrayList<>();

            synchronized (dataMap) {
                dataMap.get(id).free(list);
            }

            return list;
        } catch (NoSuchElementException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public void setActiveIds(Collection<Integer> ids) {
        activeIds = ids;
    }

    @Override
    public void close() {
        try {
            socket.close();
            socket = null;
        } catch (NullPointerException | IOException e) {}
    }

    @Override
    public void run() {
        super.run();

        int interval = context.getResources().getInteger(R.integer.request_interval);
        long nextTime = System.currentTimeMillis() + interval;

        while (!isInterrupted()) {
            Log.d("TAG", "fetching");

            try {
                fetch();
            } catch (IOException e) {
                close();
                Log.d("TAG", "fetching data from server failed: " + e);
            }

            long currentTime = System.currentTimeMillis();

            while (currentTime < nextTime && !isInterrupted()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {}

                currentTime = System.currentTimeMillis();
            }

            nextTime += interval;
        }

        close();
    }

    private void fetch() throws IOException {
        Log.d("TAG", "fetch begin");
        Map<Integer, Data> map = new TreeMap<>();

        synchronized (dataMap) {
            for (int id : activeIds) {
                Data data = dataMap.get(id);

                map.put(id, data);
            }
        }


        if (socket == null || socket.isClosed()) {
            Log.d("TAG", "opening new socket");
            socket = new Socket(address, port);
            socket.setTcpNoDelay(true);
            Log.d("TAG", "socked opened");
        }


        int maxCount = context.getResources().getInteger(R.integer.chart_width);


        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        dos.writeInt(1); // get data command
        dos.writeInt(maxCount);
        dos.writeInt(map.size());

        for (Map.Entry<Integer, Data> entry : map.entrySet()) {
            dos.writeInt(entry.getKey());
            dos.writeInt(entry.getValue().since);
        }

        for (Map.Entry<Integer, Data> entry : map.entrySet()) {
            Data data = entry.getValue();

            int stat = dis.readInt();

            if (stat != 10)
                throw new IOException("request failed!: " + stat);

            data.since = dis.readInt();

            Log.d("TAG", "new since: " + data.since);

            int count = dis.readInt();
            int values[] = new int[count];

            for (int i = 0; i < count; i++) {
                int val = dis.readShort();

                values[i] = val;


                while (data.queue.size() > maxCount)
                    data.poll();
            }

            for (int value : values)
                data.push(value);
        }
    }

    private class Data {
        Deque<Integer> queue;
        int since;

        Data() {
            queue = new ArrayDeque<>();
            since = 0;
        }

        synchronized void push(int value) {
            queue.add(value);
        }

        synchronized int poll() {
            return queue.remove();
        }

        synchronized void free(List<Integer> list) {
            while (!queue.isEmpty())
                list.add(queue.remove());
        }
    }
}
