package pl.poznan.putmotorsport.telemetria.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import putmotorsport.poznan.pl.telemetria.android.R;

public class ChartActivity extends AppCompatActivity {
    private Map<CharSequence, ChartDescription> idxMap;
    private ChartPager pager;
    private NavigationView drawer;
    private TcpClient client;

    private final NavigationView.OnNavigationItemSelectedListener listener =
            new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            ChartDescription desc = idxMap.get(item.getTitle());
            int index = Charts.indexOf(desc);

            pager.setCurrentItem(index);

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        idxMap = new TreeMap<>();

        FragmentManager fm = getSupportFragmentManager();
        ChartAdapter adapter = new ChartAdapter(fm);

        pager = findViewById(R.id.pager);
        pager.setAdapter(adapter);

        drawer = findViewById(R.id.drawer);

        for (ChartDescription desc : Charts.descriptions) {
            MenuItem item = drawer.getMenu().add(desc.title);

            item.setTitle(desc.title);

            idxMap.put(item.getTitle(), desc);
        }

        drawer.setNavigationItemSelectedListener(listener);
    }

    @Override
    protected void onStart() {
        super.onStart();

        client = new TcpClient(this);
        client.start();

        pager.setClient(client);
    }

    @Override
    protected void onStop() {
        client.interrupt();
        client.close();

        super.onStop();
    }

    AbstractTcpClient getTcpClient() {
        return client;
    }
}
