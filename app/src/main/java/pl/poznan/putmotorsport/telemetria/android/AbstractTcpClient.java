package pl.poznan.putmotorsport.telemetria.android;

import java.io.Closeable;
import java.util.Collection;
import java.util.List;

public interface AbstractTcpClient extends Closeable {
    List<Integer> getNewData(int id);

    void setActiveIds(Collection<Integer> ids);
}
