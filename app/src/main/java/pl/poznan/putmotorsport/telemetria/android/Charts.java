package pl.poznan.putmotorsport.telemetria.android;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

public class Charts {
    static final ChartDescription[] descriptions;
    static final int[] identifiers;

    static int indexOf(ChartDescription desc) {
        for (int i = 0; i < descriptions.length; i++)
            if (descriptions[i] == desc)
                return i;

        return -1;
    }

    static {
        descriptions = new ChartDescription[] {
                new ChartDescription(
                        "Prędkości kół [km/h]",
                        0, 150,
                        new LineDescription(
                                90, "lewe tył", Color.RED
                        ),
                        new LineDescription(
                                91, "prawe tył", Color.YELLOW
                        ),
                        new LineDescription(
                                81, "lewe przód", Color.CYAN
                        ),
                        new LineDescription(
                                80, "prawe przód", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "Prędkość GPS [km/h]",
                        0, 150,
                        new LineDescription(
                                110, "Prędkość", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "Ciśnienie hamulców [0 -1024]",
                        0, 1024,
                        new LineDescription(
                                62, "przód", Color.RED
                        ),
                        new LineDescription(
                                63, "tył", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "TPS [%]",
                        0, 100,
                        new LineDescription(
                                41, "TPS [%]", Color.YELLOW
                        )
                ),
                new ChartDescription(
                        "Obroty silnika",
                        0, 15000,
                        new LineDescription(
                                40, "RPM", Color.RED
                        )
                ),
                new ChartDescription(
                        "Bieg",
                        0, 3,
                        new LineDescription(
                                53, "Bieg", Color.WHITE
                        )
                ),
                new ChartDescription(
                        "Lambda [y X100]",
                        0, 200,
                        new LineDescription(
                                43, "Lambda [y x100]", Color.CYAN
                        )
                ),
                new ChartDescription(
                        "Temperatura [C]",
                        0, 200,
                        new LineDescription(
                                50, "olej", Color.RED
                        ),
                        new LineDescription(
                                52, "CLT", Color.CYAN
                        )
                ),
                new ChartDescription(
                        "Ciśnienie [Bar x10]",
                        0, 50,
                        new LineDescription(
                                51, "paliwa", Color.RED
                        ),
                        new LineDescription(
                                42, "oleju", Color.YELLOW
                        )
                ),
                new ChartDescription(
                        "Ugięcie amortyzatorów [%]",
                        0, 1024,
                        new LineDescription(
                                23, "lewy tył", Color.RED
                        ),
                        new LineDescription(
                                22, "prawy tył", Color.YELLOW
                        ),
                        new LineDescription(
                                61, "lewy przód", Color.CYAN
                        ),
                        new LineDescription(
                                60, "prawy przód", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "Położenie kierownicy",
                        -120, 120,
                        new LineDescription(
                                82, "kąt skrętu", Color.CYAN
                        )
                ),
                new ChartDescription(
                        "Napięcie akumulatora [V x100",
                        600, 1600,
                        new LineDescription(
                                130, "Napięcie [V x100]", Color.RED
                        )
                ),
                new ChartDescription(
                        "Akcelerometr [g x100]",
                        -600, 600,
                        new LineDescription(
                                111, "Oś X", Color.RED
                        ),
                        new LineDescription(
                                112, "Oś Y", Color.YELLOW
                        ),
                        new LineDescription(
                                113, "Oś Z", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "Żyroskop",
                        -500, 500,
                        new LineDescription(
                                121, "Oś X", Color.RED
                        ),
                        new LineDescription(
                                122, "Oś Y", Color.YELLOW
                        ),
                        new LineDescription(
                                123, "Oś Z", Color.GREEN
                        )
                ),
                new ChartDescription(
                        "random",
                        0, 50,
                        new LineDescription(
                                0, "wartość", Color.WHITE
                        )
                )
        };

        List<Integer> all = new ArrayList<>();

        for (ChartDescription cd : Charts.descriptions)
            for (LineDescription ld : cd.lines)
                all.add(ld.id);

        identifiers = new int[all.size()];

        for (int i = 0; i < all.size(); i++)
            identifiers[i] = all.get(i);
    }
}
