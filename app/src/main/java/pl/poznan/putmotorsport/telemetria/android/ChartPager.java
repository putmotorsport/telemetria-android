package pl.poznan.putmotorsport.telemetria.android;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Collection;

public class ChartPager extends ViewPager {
    private TcpClient client;
    private int primaryPage;
    private int secondaryPage;

    private final OnPageChangeListener pageListener = new OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
            int pri = i;
            int sec = v > 0.01 ? i + 1 : -1;

            setActiveIds(pri, sec);
        }

        @Override
        public void onPageSelected(int i) {}

        @Override
        public void onPageScrollStateChanged(int i) {}
    };

    public ChartPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        addOnPageChangeListener(pageListener);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    void setClient(TcpClient newClient) {
        client = newClient;

        setActiveIds();
    }

    private void setActiveIds(int primary, int secondary) {
        if (client == null)
            return;

        if (primary == primaryPage && secondary == secondaryPage)
            return;

        primaryPage = primary;
        secondaryPage = secondary;

        setActiveIds();
    }

    private void setActiveIds() {
        Collection<Integer> ids = new ArrayList<>();

        addLineIds(ids, primaryPage);
        addLineIds(ids, secondaryPage);

        if (client != null)
            client.setActiveIds(ids);
    }

    private void addLineIds(Collection<Integer> ids, int index) {
        try {
            ChartDescription cd = Charts.descriptions[index];

            for (LineDescription ld : cd.lines)
                ids.add(ld.id);
        } catch (IndexOutOfBoundsException e) {
            // nothing to append
        }
    }
}
